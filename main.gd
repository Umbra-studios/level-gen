extends Spatial

export (String,FILE) var player_scene 
onready var room_generator = get_node("room_generator")
var thread=Thread.new()
func _ready():
	thread.start(room_generator,"generate_rooms",[thread,room_generator])
	# room_generator.generate_rooms([null,room_generator])
	room_generator.connect("level_generated", self, "_on_room_generated")

func _on_room_generated():
	get_node("camera").current = false
	var player = load(player_scene).instance()
	add_child(player)
	player.global_transform.origin = get_node("player_spawn").global_transform.origin
	player.get_node("helper/camera").current = true

func restart():
	get_tree().reload_current_scene()
