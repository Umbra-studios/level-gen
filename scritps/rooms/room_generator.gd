extends Spatial
class_name RoomsGenerator

const INTERVAL = 0.1

signal level_generated

var room_start_scn 
var room_end_scn 
export var tile_set:Resource setget set_tile
# Room scene and chance of spawning
var rooms
var loot_rooms
var secret_room
var event_rooms
var iteration_range

var available_doorways = []

var room_start 
var room_end 
var placed_rooms = []

func _ready():
	set_tile(tile_set)
func set_tile(val):
	tile_set=val
	room_start_scn=tile_set.start_rooms
	room_end_scn=tile_set.end_rooms
	rooms=tile_set.rooms
	event_rooms=tile_set.event_rooms
	secret_room=tile_set.secret_rooms
	loot_rooms=tile_set.loot_rooms
	iteration_range=tile_set.iteration_range

func generate_rooms(val):
	# Add start room
	room_start = room_start_scn[int(rand_range(0,room_start_scn.size()-1))].instance()
	val[1].add_child(room_start)
	room_start.global_transform.origin = global_transform.origin
	add_doorways_to_available_doorways(room_start)
	room_start.create_random_objects()
	yield(get_tree().create_timer(INTERVAL), "timeout")
	
	# Add corridors
	randomize()
	var iterations := int(rand_range(int(iteration_range.x), int(iteration_range.y)))
	var main_room:=int(iterations*rand_range(0.4,0.6))
	var secret_room_pos:=int(rand_range(main_room*0.5,main_room*1.5))
	var temp
	for i in iterations:
		var current_room 
		if val.size()>2 and main_room%2!=0:
			temp=get_room_event(val[2])
			if temp==null:
				continue
			current_room= temp.instance()
		else:	
			if secret_room_pos !=i:
				if main_room!=0:
					current_room= get_rand_room().instance()
					main_room-=1
				else:
					temp=get_rand_room(2)
					if temp==null:
						continue
					current_room= temp.instance()
			else:
				temp=get_rand_room(3)
				if temp==null:
					continue
				current_room= temp.instance()
		val[1].add_child(current_room)
		var room_placed = false
		for adi in available_doorways.size():
			for cdi in current_room.doorways.size():
				position_room_at_doorway(current_room, current_room.doorways[cdi], available_doorways[adi])
				yield(get_tree().create_timer(INTERVAL), "timeout")
				if !check_room_overlap(current_room):
					room_placed = true
					placed_rooms.push_back(current_room)
					
					current_room.doorways[cdi].queue_free()
					current_room.doorways.remove(cdi)
					
					available_doorways[adi].queue_free()
					available_doorways.remove(adi)
					break
			if room_placed:
				add_doorways_to_available_doorways(current_room)
				break
		if !room_placed:
			current_room.queue_free()
		else:
			current_room.create_random_objects()
			
	
	# Add end room
	room_end = 	room_end_scn[int(rand_range(0,room_end_scn.size()-1))].instance()
	val[1].add_child(room_end)
	room_end.global_transform.origin = global_transform.origin
	var room_placed = false
	for adi in available_doorways.size():
		position_room_at_doorway(room_end, room_end.doorways[0], available_doorways[adi])
		yield(get_tree().create_timer(INTERVAL), "timeout")
		if !check_room_overlap(room_end):
			room_placed = true
			room_end.doorways[0].queue_free()
			available_doorways[adi].queue_free()
			available_doorways.remove(adi)
			break
	if !room_placed:
		room_end.queue_free()
		clear_start(val)
	room_end.create_random_objects()
	
	if val[0]!=null:
		if val[0].is_active():
			val[0].wait_to_finish()
	emit_signal("level_generated",val[1].get_children())

	
func position_room_at_doorway(room : Room, room_doorway : Doorway, target_doorway : Doorway):
	if is_instance_valid(room_doorway) and is_instance_valid(target_doorway):
		# Reset room's position and rotation
		room.global_transform.origin = Vector3.ZERO
		room.rotation = Vector3.ZERO
		
		# Rotate the room
		var target_doorway_euler = target_doorway.global_transform.basis.get_euler()
		var room_doorway_euler = room_doorway.global_transform.basis.get_euler()
		var delta_angle = delta_angle(room_doorway_euler.y, target_doorway_euler.y)
		var current_room_target_rotation : Quat = Quat(Vector3.UP, delta_angle)
		room.global_transform.basis = current_room_target_rotation.inverse()
		
		# Position the room
		var room_position_offset = room_doorway.global_transform.origin - room.global_transform.origin
		room.global_transform.origin = target_doorway.global_transform.origin - room_position_offset

func check_room_overlap(room : Room):
	return room.overlaps

func get_rand_room(type=1):
	var list=[]
	var new_rooms=[]
	if type==0:
		new_rooms=room_start_scn
	elif type==1:
		new_rooms=rooms
	elif type==2:
		new_rooms=loot_rooms
	elif type==3:
		new_rooms=secret_room
	elif type==5:
		new_rooms=room_end_scn
	for i in new_rooms:
		list.append(i)
	if list.empty():
		return null
	return list[int(rand_range(0,list.size()-1))]

func add_doorways_to_available_doorways(room : Room):
	for d in room.doorways:
		available_doorways.push_back(d)
func get_room_event(index):
	for i in event_rooms.size():
		if i==index:
			return event_rooms[i]
	return null
func clear_start(val):
	placed_rooms = []
	available_doorways = []
	for n in val[1].get_children():
		n.queue_free()
	if val[0]!=null:
		generate_rooms(val)

func delta_angle(current : float, target : float)->float:
	var delta : float = repeat((target - current), 360.0)
	if delta > 180.0:
		delta -= 360.0
	return delta

func repeat(t : float, length : float)->float:
	return clamp(t - floor(t / length) * length, 0.0, length)
