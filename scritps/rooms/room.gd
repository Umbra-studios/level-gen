extends Spatial
class_name Room
enum room_type{start,normal,loot,secret,event,end}
export (room_type)var type
var doorways := []
var random_objects:=[]
export var doorways_path:NodePath
export var random_objects_path:NodePath
export var bounds_path:NodePath
export (Array,PackedScene)var objects:=[]
export var player_spawn:NodePath
export var enemy_spawn:NodePath
export var event_area:NodePath
var drop_tables
var bounds : Area
var overlaps : bool = false
var is_player_in:=false

func _ready():
	pause_mode=2
	doorways = get_node(doorways_path).get_children()
	bounds=get_node(bounds_path)
	if get_node(event_area)!=null:
		get_node(event_area).connect("body_entered",self,"event_handle")
		
	if bounds!=null:
		bounds.connect("body_entered",self,"player_in")
		bounds.connect("body_exited",self,"player_out")

func _process(delta):
	if bounds.get_overlapping_areas().size() > 0:
		overlaps = true
	else:
		overlaps = false

func create_random_objects():
	if get_node_or_null(random_objects_path)!=null:
		for i in get_node(random_objects_path).get_children():
			if objects.empty():
				return
			var object=objects[int(round(rand_range(0,objects.size()-1)))].instance()
			i.add_child(object)
			if object.get("drop_tables")!=null:
				object.drop_tables=drop_tables
				
func player_in(body):
	if body.is_in_group("player"):
		is_player_in=true
func player_out(body):
	if body.is_in_group("player"):
		is_player_in=false
func event_handle(body):
	pass