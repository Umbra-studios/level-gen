extends Resource

class_name tile
export (Array,PackedScene) var start_rooms
export(Array,PackedScene) var rooms
export(Array,PackedScene) var loot_rooms
export(Array,PackedScene) var event_rooms
export(Array,PackedScene) var secret_rooms
export(Array,PackedScene) var end_rooms
export (Array,Resource) var loot_table
export var iteration_range : Vector2 = Vector2(5, 15)