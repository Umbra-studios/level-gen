extends Resource
class_name loot_table

export var droped_items:=Vector2()
export(Array,Array) var loot

func drop():
	var list:=[]
	var iterations:=int(round(rand_range(droped_items.x,droped_items.y)))
	while iterations>0:
		for i in loot:
			if rand_range(0,100)<i[1]:
				list.append(i[0].duplicate())
				iterations-=1
	return list
