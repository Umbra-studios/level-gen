extends Spatial

var in_put:=false
export var loot_table:Resource 
func _on_Area_body_entered(body):
	if body.name=="player":
		in_put=true
func _on_Area_body_exited(body):
	if body.name=="player":
		in_put=false

func _process(delta):
	if in_put:
		if Input.is_action_just_pressed("ui_accept"):

			print(loot_table.drop())
			queue_free()

